// 观察者类
class Observer {
  constructor() {
    this.subNode = []
  }
  //添加观察者类
  addSubNode (node) {
    this.subNode.push(node)
  }
  //通知修改
  update (newVal) {
    this.subNode.forEach(node => {
      node.value = newVal
      node.innerHTML = newVal
    })
  }
}