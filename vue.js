class Vue {
  constructor(opt) {
    this.opt = opt
    this.observe(opt.data)
    let root = document.querySelector(opt.el)
    this.compile(root)

  }
  // 为每个Data中的每个Key 绑定一个观察者对象
  observe (data) {
    Object.keys(data).forEach(key => {
      let obv = new Observer()
      data['_' + key] = data[key]
      // 通过 getter setter 暴露 for 循环中作用域下的 obv，闭包产生
      Object.defineProperty(data, key, {
        get () {
          // 添加订阅    
          Observer.target && obv.addSubNode(Observer.target)
          return data['_' + key]
        },
        set (newVal) {
          obv.update(newVal)
          data['_' + key] = newVal
        }
      })
    })
  }
  // 初始化页面,遍历DOM,收集每个Key变化,以观察者方式存放起来
  compile (node) {
    [].forEach.call(node.children, child => {
      if (/\{\{(.*)\}\}/.test(child.innerHTML)) {
        // 匹配双{{}}表达式
        let key = RegExp.$1.trim()
        // 更改实际的值
        child.innerHTML = child.innerHTML.replace(new RegExp('\\{\\{\\s*' + key + '\\s*\\}\\}'), this.opt.data[key])
        // 设置 Observer.target，表示当前正在处理的 DOM 元素
        Observer.target = child
        // 触发 getter，从而将当前 DOM 元素加入到观察者中
        this.opt.data[key]
        // 重置 Observer.target，避免影响其他 DOM 元素
        Observer.target = null
      }
      else if (child.attributes.length > 0) {
        this.bindevent(child)
      }
      else {
        // 递归处理子节点 
        this.compile(child)
      }
    })
  }
  bindevent (child) {
    [].forEach.call(child.attributes, info => {
      let name = info.name
      switch (name) {
        case 'v-model': {
          let key = child.getAttribute(name)
          child.value = this.opt.data[key]
          let a = child.tagName.toLowerCase()
          child.addEventListener(a, event => {
            this.opt.data[key] = event.target.value
          })
          child.innerHTML = child.innerHTML.replace(new RegExp('\\{\\{\\s*' + key + '\\s*\\}\\}'), this.opt.data[key])
          Observer.target = child
          this.opt.data[key]
          Observer.target = null
          break
        }
        case 'v-html': {
          let key = child.getAttribute(name)
          child.innerHTML = this.opt.data[key]
          Observer.target = child
          this.opt.data[key]
          Observer.target = null
          break
        }
        case 'v-click': {
          let key = child.getAttribute(name)
          // 设置 Observer.target，表示当前正在处理的 DOM 元素
          child.addEventListener('click', this.opt.methods[key])
        }
      }
    })
  }
}